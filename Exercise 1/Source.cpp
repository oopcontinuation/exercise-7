#include <iostream>



/* Calculate distance on time
* (a) acceleration m/s2
* (v) starting velocity m/s
* (r) starting distance m
* (t) time s
*
* 1/2 * a * t^2 + v * t + r
*/
auto distance = [](float a, float v, float r, float t) {
	return 0.5 * a * t * t + v * t + r;
};

int main() {
	
	std::cout << distance(10, 20, 0, 30);

	system("pause");
	return 0;
}

