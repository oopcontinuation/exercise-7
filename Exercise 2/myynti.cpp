#include "myynti.h"

using namespace std;
myynti::myynti(const string& rivi) {
	stringstream ss(rivi);
	ss >> aika_ >> hinta_ >> m��r�_ >> ostaja_ >> myyj�_;
}

float myynti::hinta() const { return hinta_; }
int myynti::m��r�() const { return m��r�_; }
string myynti::ostaja() const { return ostaja_; }
string myynti::myyj�() const { return myyj�_; }
string myynti::aika() const { return aika_; }