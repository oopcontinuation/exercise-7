#include <iostream>
#include <fstream>
#include <map>

#include "myynti.h"

using namespace std;

auto incr = [](int& i) {
	i++;
};

int main(){
	ifstream file("nokia18032009.txt");
	map<string, int> myynnit;
	string line;
	getline(file, line); // Hyp�� header rivin yli

	while (!file.eof()){
		getline(file, line);
		const myynti myynti(line);

		// Onko myyj� ja ostaja samoja
		if (myynti.myyj�() == myynti.ostaja()){
			if (myynnit.count(myynti.myyj�()) == 0){
				myynnit.insert(pair<string, int>(myynti.myyj�(), 1));
			}
			else {
				// Jos jo myynnit s�ili�ss�, incrementoi m��r��
				incr(myynnit.find(myynti.myyj�())->second);
			}
		}
	}

	cout << "Myyj\204" << "\t" << "M\204\204r\204" << endl;
	for (const auto& s : myynnit){
		cout << s.first << "\t" << s.second << endl;
	}

	system("pause");
	return 0;
}