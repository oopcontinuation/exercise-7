#ifndef MYYNTI_H
#define MYYNTI_H

#include <sstream>
#include <string>

class myynti {
private:
	float hinta_ = 0;
	int m��r�_ = 0;
	std::string ostaja_;
	std::string myyj�_;
	std::string aika_;
public:

	explicit myynti(const std::string& data);

	float hinta() const;
	int m��r�() const;
	std::string ostaja() const;
	std::string myyj�() const;
	std::string aika() const;
};
#endif